//
//  ContentView.swift
//  Startrek Radio
//
//  Created by Roman Putintsev on 31.03.2021.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, Startrek Radio!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
