//
//  Startrek_RadioApp.swift
//  Startrek Radio
//
//  Created by Roman Putintsev on 31.03.2021.
//

import SwiftUI

@main
struct Startrek_RadioApp: App {
    var player: STStartrekPlayer? = nil

    init() {
        STStartrekNetwork.setReferer("com.infteh.startrekradio.ios")
        player = STStartrekPlayer.create()
        //        player?.setDaastUrl("http://a.adwolf.ru/2909/getCode?pp=do&ps=clc&p2=jg")
        player?.playUrl("https://hls-02-europaplus.emgsound.ru/11/playlist.m3u8")
    }

    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
